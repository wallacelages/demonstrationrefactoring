

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public delegate void HandleEvent(int idx, bool status);

public class CapsuleListener : MonoBehaviour  
{
	private Quaternion rotation;
	private Quaternion calibration;
	private Quaternion grab;

	private int  offset = 0;

	public int handle1 = 0;
	public int handle2 = 0;

	public bool handle1_pressed = false;
	public  bool handle2_pressed = false;

	public int handle2_thres = 20;
	public int handle1_thres = 130;

	public int slider_min_thres = 15;
	public int slider_max_thres = 200;

	public bool capsuleDetected = false;

	public float slider = 0.0f;

	HandleEvent handleEvent;

	public void Start()
	{
		calibration = Quaternion.identity;
		grab = Quaternion.identity;

		//create a father to store callibration and make capsule a son
		GameObject capsuleCalibration = new GameObject ();
		this.transform.SetParent (capsuleCalibration.transform);
		capsuleCalibration.name = "CapsuleCallibration";

	}

	public float GetSliderPosition()
	{

		return slider;

	}

	public void AddHandleCallback( HandleEvent ev)
	{
		handleEvent += ev;
	}

	public void Update()
	{
		//Keyboard shortcuts

		//Callibration
		if(Input.GetKeyDown ("c"))
		{
			calibration = Quaternion.Inverse(rotation);
			this.transform.parent.localRotation = calibration;
			Debug.Log("Calibration");	
		}

		//Handle press
		if(Input.GetKeyDown ("l"))
		{
			handleEvent (1,true);
		}

		if(Input.GetKeyDown ("r"))
		{
			handleEvent (2,true);	
		}

		//Offset
		if(Input.GetKeyDown ("["))
		{			
			slider -= 0.1f;
			if (slider < 0.0f)
				slider = 0.0f;
		}

		if(Input.GetKeyDown ("]"))
		{
			slider+= 0.1f;
			if (slider > 1.0f)
				slider = 1.0f;
		}



		//Rotation
		if (!capsuleDetected) {
			float h = Input.GetAxis ("Horizontal");
			float v = Input.GetAxis ("Vertical");
			this.transform.Rotate (v * Vector3.right + h * Vector3.down);
		}
		//--------------------------------------
		//Handle1 Pressed
		//------------------------------------
		if((handle1 > handle1_thres) && (handle1_pressed == false))
		{
			handleEvent (1,true);
			handle1_pressed = true;
		}
		else if ((handle1 < (handle1_thres*0.90)) && (handle1_pressed == true))
		{
			handleEvent (1,false);
			handle1_pressed = false;
		}

		//--------------------------------------
		//Handle2 Pressed
		//------------------------------------
		if((handle2 > handle2_thres) && (handle2_pressed == false))
		{
			handleEvent (2,true);
			handle2_pressed = true;
		}
		else if ((handle2 < (handle2_thres*0.90)) && (handle2_pressed == true))
		{
			handleEvent (2,false);
			handle2_pressed = false;
		}
			
		if (capsuleDetected)
		this.transform.localRotation = rotation;
	}

	public void OSCMessageReceived(OSC.NET.OSCMessage message)
	{	
		string address = message.Address;
		ArrayList args = message.Values;

		float w = (float) args[0];
		float y = (float) args[1];
		float x = (float) args[2];
		float z = (float) args[3];
		handle1 = (int) args[4];
		handle2 = (int) args[5];
		offset = (int) args[6];

		//this.transform.localRotation.Set(x,y,z,w);
		rotation.Set(x,y,z,w);
		//OSGUBG

		Debug.Log (offset);
		//--------------------------------------
		//Offset
		//------------------------------------
		if ((offset > slider_max_thres) || (offset < slider_min_thres))
			slider = 0;
		else slider = (float) (offset - slider_min_thres)/ (float) (slider_max_thres - slider_min_thres);

		if (slider > 1.0f)
			slider = 1.0f;

		capsuleDetected = true;
		Debug.Log("data: " + x + " " + y + " " + z + " " + w + " " + handle1 + " " + handle2 + " " + offset);	
	}
}
