﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VolumeManager : MonoBehaviour {

	private const int ROTATE = 1;
	private const int SPLIT = 2;
	private const int TRANSLATE = 3;
	private const int NONE = 0;

	public GameObject Capsule;
	public GameObject Volume;

	private VolumeCube volumeCube;

	private CapsuleListener capsule;

	public GameObject Program;

	private ProgramManager program;

	public int selectMode = 0;   //0 -> no selection (sub selection) 1->object selection
	public int operationMode = NONE;

	private List<List <Quaternion>> recordings;
	private int recordIndex = 0;

	private List<Quaternion> rotation;
	public double capturePeriod = 0.02;
	private double lastTime = 0;
	private int playIndex = 0;
	private State state;

	public int currentRecordTime = 0;
	private int recordPlaying = 0;
	private int currentPlayingTime = 0;
	public GameObject replay;

	Image recordButton;


	enum State{RECORDING, PLAYING, NONE};

	// Use this for initialization
	void Start () {
	
		capsule = Capsule.GetComponent<CapsuleListener> ();
		volumeCube = Volume.GetComponent<VolumeCube> ();
		program = Program.GetComponent<ProgramManager> ();

		capsule.AddHandleCallback (new HandleEvent (HandlePressed));

		recordings = new List< List<Quaternion> >();


		state = State.NONE;

	}

	void HandlePressed(int idx, bool state)
	{
		if (state == false)
			return;

		//selection mode
		if (idx == 1) {
			selectMode++;

			//loop mode
			if (selectMode > volumeCube.NumNodes())
				selectMode = 0;

			program.Add ("n" + selectMode + " = " + selectMode + "\n");

			volumeCube.Grab(selectMode, capsule.transform);   //grab everything

		}

		//operation mode
		if (idx == 2) 
		{
			//if there is a group selected
			if(selectMode != 0)
			{
				
				//if was splitting, finish splitting record result
				if (operationMode == SPLIT) {
					program.Add ("n2,n3 = time(" + currentRecordTime + ",split," + "n"+selectMode + "," + capsule.GetSliderPosition () +")\n");
					currentRecordTime++;
					volumeCube.EndSplit ();

				}

				operationMode++;

				//if was not splitting, start splitting 
				if (operationMode == SPLIT) {
					volumeCube.BeginSplit ();
				}

				//loop mode
				if (operationMode > 3)
					operationMode = NONE;
			}			
				
		}


	}
	//-------------------------------LUA CALLBACKS
	public bool isTime(float t)
	{
		if (t == currentPlayingTime) {
			Debug.Log ("time is now");
			return true;
		}
		else
			return false;
	}		
	public void Rotate(int obj, int record)
	{
		Debug.Log ("called rotate");
		Replay (obj,record);
	}


	public void Split(int obj, float point)
	{
		Debug.Log ("called split");
		currentPlayingTime++;
	}

	//-------------------------------------------------------

	// Update is called once per frame
	void Update () {
	
		//if there is a group selected and operation is split begin splitting
		if ((operationMode == SPLIT) && (selectMode != 0) ) 
			volumeCube.Select (capsule.GetSliderPosition ());

	}

	// Update is called once per frame
	void FixedUpdate () {

		if (Time.realtimeSinceStartup - lastTime > capturePeriod) {

			if (state == State.RECORDING) {
				rotation.Add (capsule.transform.rotation);

			} else if (state == State.PLAYING) {

				program.Call ("update");

				if (recordIndex > 0) {
					if (playIndex < rotation.Count) {
						//state = State.NONE;
					//	recordButton.color = Color.white;
					//} else {
						replay.transform.rotation = rotation [playIndex];
						playIndex++;
						currentPlayingTime++;
					}
				}


			}

			lastTime = Time.realtimeSinceStartup;
		}


	}

	public void onRecordClick()
	{
		Image b = EventSystem.current.currentSelectedGameObject.GetComponent<Image> ();

		//stopping
		if (state == State.RECORDING) {
			state = State.NONE;
			b.color = Color.white;
			program.Add ("time(" + currentRecordTime + ",rotate"+ ",n" + selectMode + "," + recordIndex +  ")\n");
			recordings.Add(rotation);
			recordIndex++;
			currentRecordTime += rotation.Count;
		}
		else { //recording
			rotation = new List<Quaternion> ();
			state = State.RECORDING;
			b.color = Color.red;
		}
	}

	public int Replay(int obj, int record)
	{
		playIndex = 0;
		rotation = recordings [record];

		selectMode = 0;
		volumeCube.Grab(0, capsule.transform);
		volumeCube.Grab(obj, replay.transform); 
		Debug.Log ("replay" + " " + obj + " " + record);

		return (int) (rotation.Count / capturePeriod);

	}

	public void onRunClick()
	{


		recordButton = EventSystem.current.currentSelectedGameObject.GetComponent<Image> ();

		Debug.Log (state);

		if (state == State.PLAYING) {
			state = State.NONE;
			recordButton.color = Color.white;
			Debug.Log ("stoppin");
		//	Capsule.SetActive (true);
		}
		else {
	
			state = State.PLAYING;
		//	Capsule.SetActive (false);
			recordButton.color = Color.green;
			program.Run ();
			currentPlayingTime = 0;
			selectMode = 0;
			replay.transform.rotation = Quaternion.identity;
			volumeCube.Grab(selectMode, capsule.transform);   //grab everything
			volumeCube.Revert ();
			Debug.Log ("runnig");
		}
	}

}
