﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using NLua;

public class ProgramManager : MonoBehaviour {
	
	public GameObject texField;
	private InputField inputField;
	public GameObject volumeManager;
	private VolumeManager manager; 

	Lua env;

		private void SubmitName(string arg0)
		{
			Debug.Log(arg0);
		}

	void Awake() {
		env = new Lua();
		env.LoadCLRPackage();

		env["transform"] = transform;

	}
		

	public void Add( string cmd)
	{
		inputField.text += cmd;

	}

	void Start() {

		inputField = texField.GetComponent<InputField>();
		var se = new InputField.SubmitEvent();

		manager = volumeManager.GetComponent<VolumeManager> ();
		env["this"] = manager; // Give the script access to the gameobject.


		se.AddListener(SubmitName);
		inputField.onEndEdit = se;
		inputField.text = "import 'System'" + "\n" + "import 'UnityEngine'" + "\n" + "import 'Assembly-CSharp'"
		+ "\n" + "\n" + "function time(t,f,a,b)\n  if this:isTime(t) then \n    return f(a,b)\n  end \nend \n\n"
		+ "function split(obj,par)\n  this:Split(obj, par)\n return obj+1,obj+2 \n end\n\n" +
		"function rotate(obj,par)\n  this:Rotate(obj,par)\n return 0,0 \n end\n\n" + "n1 = 1\n\n"
			+ "function update()\n\n";
	}

	public void Run()
	{
		try {
			env.DoString(inputField.text + "\nend");
		} catch(NLua.Exceptions.LuaException e) {
			Debug.LogError(FormatException(e), gameObject);
		}

		Call("Start");

	}

	void Update() {
	//	Call("Update");
	}

	void OnGUI() {
	//	Call("OnGUI");
	}

	public System.Object[] Call(string function, params System.Object[] args) {
		System.Object[] result = new System.Object[0];
		if(env == null) return result;
		LuaFunction lf = env.GetFunction(function);
		if(lf == null) return result;
		try {
			// Note: calling a function that does not
			// exist does not throw an exception.
			if(args != null) {
				result = lf.Call(args);
			} else {
				result = lf.Call();
			}
		} catch(NLua.Exceptions.LuaException e) {
			Debug.LogError(FormatException(e), gameObject);
		}
		return result;
	}

	public System.Object[] Call(string function) {
		return Call(function, null);
	}

	public static string FormatException(NLua.Exceptions.LuaException e) {
		string source = (string.IsNullOrEmpty(e.Source)) ? "<no source>" : e.Source.Substring(0, e.Source.Length - 2);
		return string.Format("{0}\nLua (at {2})", e.Message, string.Empty, source);
	}
}
