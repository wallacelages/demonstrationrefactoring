﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VolumeCube : MonoBehaviour {

	public GameObject voxelPrefab;
	public int size = 4;
	public float spacing = 1f;

	public Material defaultMaterial;
	public Material selectedMaterial;

	private GameObject selectedGroup;
	private GameObject [,,]volume;

	private GameObject newchild,oldchild;
	private GameObject root;

	private GameObject originalParent;

	private int numNodes = 1;

//	private List<GameObject> tree;

	private float selectionSpan = 0.0f;
	public int groupIndexer = 1;

	bool splitting = false;

	public float getPercentage()
	{
		return selectionSpan;
	}

	// Use this for initialization
	void Start () {

		root = new GameObject ();
		root.transform.SetParent (this.transform);
		root.name = "1";

		selectedGroup = root.gameObject;
		originalParent = selectedGroup.transform.parent.gameObject;


		volume = new GameObject[2*size, 2*size, 2*size];

		for (int y = -size; y != size; y++) {
			for (int x = -size;  x != size; x++) {
				for (int z = -size;  z != size; z++) {
					Vector3 pos = new Vector3((float) x, (float) y, (float) z) * spacing;
					GameObject obj  = (GameObject) Instantiate(voxelPrefab, pos, Quaternion.identity);
					obj.transform.SetParent (root.transform);
					volume[x+size,y+size,z+size] = obj;
				}
			}
		}

	}

	public void Select(float percentage)
	{

		if (!splitting)
			return;
		
		selectionSpan = percentage;
	/*	for (int y = -size; y != size; y++) {
			for (int x = -size;  x != size; x++) {
				for (int z = -size;  z != size; z++) {
					volume [x + size, y + size, z + size].GetComponent<Renderer> ().material = defaultMaterial;
					volume [x + size, y + size, z + size].transform.SetParent (oldchild.transform);

				}
			}
		}*/

		Deselect(true);

		int endSelection = (int) (-size + 2 * size * percentage);

		for (int y = -size; y!= size; y++) {
			for (int x = -size; x != endSelection; x++) {
				for (int z = -size; z != size; z++) {
					if (volume [x + size, y + size, z + size].transform.parent.name == oldchild.name) {
						volume [x + size, y + size, z + size].transform.SetParent (newchild.transform);	
						volume [x + size, y + size, z + size].GetComponent<Renderer> ().material = selectedMaterial;
					}
				}

			}
		}


	}

	public void EndSplit()
	{
		numNodes += 2;
		Deselect (false);
		splitting = false;
	
	}

	public void BeginSplit()
	{
		
		//insert empty node
		GameObject interNode = new GameObject ();
		interNode.name = "empty"; //groupIndexer.ToString();
		interNode.transform.SetParent (selectedGroup.transform.parent.transform);	
	
		//move current as a child
		groupIndexer++;
		selectedGroup.transform.SetParent (interNode.transform);
		string old_name = selectedGroup.name;
		selectedGroup.name = groupIndexer.ToString();

		interNode.name = old_name; 

		//add another child
		groupIndexer++;
		newchild = new GameObject ();
		newchild.name = groupIndexer.ToString(); //groupIndexer.ToString();
		newchild.transform.SetParent (interNode.transform);	

		oldchild = selectedGroup;
		selectedGroup = interNode;

		splitting = true;
		Debug.Log ("begin split");

	}

	public int NumNodes()
	{
		return numNodes;

	}

	public void Revert()
	{

		for (int y = -size; y != size; y++) {
			for (int x = -size;  x != size; x++) {
				for (int z = -size;  z != size; z++) {
					Vector3 pos = new Vector3((float) x, (float) y, (float) z) * spacing;
					volume[x+size,y+size,z+size].transform.position = pos;
					volume [x + size, y + size, z + size].transform.rotation = Quaternion.identity;
				}
			}
		}



	}


	public void Grab(int number, Transform t)
	{

			
		selectedGroup.transform.SetParent (originalParent.transform);

		if (number > 0) {	
			selectedGroup = this.transform.FindDeepChild (number.ToString ()).gameObject;
			originalParent = selectedGroup.transform.parent.gameObject;
			selectedGroup.transform.SetParent (t);
		}

	}


	public void GrabSelected(Transform t)
	{

		selectedGroup.transform.SetParent (t);

	}

	//if false just change the material back
	public void Deselect(bool tree)
	{
			foreach (Transform child in newchild.transform) {
			if(tree)
				child.SetParent (oldchild.transform);
			child.gameObject.GetComponent<Renderer> ().material = defaultMaterial;

		}
	}


	// Update is called once per frame
	void Update () {
	
	}
}
